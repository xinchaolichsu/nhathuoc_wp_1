<?php
require_once('wp_bootstrap_navwalker.php');
?>


<?php
/**
 * DaKhoaHungDung functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package DaKhoaHungDung
 */

if (!function_exists('dakhoahungdung_setup')) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function dakhoahungdung_setup()
  {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on DaKhoaHungDung, use a find and replace
     * to change 'dakhoahungdung' to the name of your theme in all the template files.
     */
    load_theme_textdomain('dakhoahungdung', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
      'menu-1' => esc_html__('Primary', 'dakhoahungdung'),
    ));

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));

    // Set up the WordPress core custom background feature.
    add_theme_support('custom-background', apply_filters('dakhoahungdung_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    )));

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');
  }
endif;
add_action('after_setup_theme', 'dakhoahungdung_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dakhoahungdung_content_width()
{
  $GLOBALS['content_width'] = apply_filters('dakhoahungdung_content_width', 640);
}

add_action('after_setup_theme', 'dakhoahungdung_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dakhoahungdung_widgets_init()
{
  register_sidebar(array(
    'name' => esc_html__('Sidebar', 'dakhoahungdung'),
    'id' => 'sidebar-1',
    'description' => esc_html__('Add widgets here.', 'dakhoahungdung'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>',
  ));
}

add_action('widgets_init', 'dakhoahungdung_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function dakhoahungdung_scripts()
{
  wp_enqueue_style('dakhoahungdung-style', get_stylesheet_uri());

  wp_enqueue_script('dakhoahungdung-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

  wp_enqueue_script('dakhoahungdung-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}

add_action('wp_enqueue_scripts', 'dakhoahungdung_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* custom */
/* Tao location cho menu, khong phai menu */
if (!function_exists('custom_menu')) {
  function custom_menu($slug)
  {
    $menu = array(
      'theme_location' => $slug,
      'container' => 'nav',
      'container_class' => $slug,
//            'menu_class' => 'myclass',
//            'menu_id' => 'myid'
    );
    wp_nav_menu($menu);
  }
}


register_nav_menu('mainmenu', 'Menu Chinh');


add_action('init', 'dakhoahungdung_register_my_post_types');
function dakhoahungdung_register_my_post_types()
{



  $args1 = array(
    'public' => true,
    'has_archive' => true,
    'labels' => array('name' => 'Tin tức'),
    'taxonomies' => array('category'),
    'rewrite' => array('slug' => 'tintuc'),
    'custom_fields' => true,
    'supports' => array('title', 'editor', 'author',
      'thumbnail', 'comments', 'custom_fields', 'excerpt')
  );
  register_post_type('tintuc', $args1);


  $args2 = array(
    'public' => true,
    'has_archive' => true,
    'labels' => array('name' => 'Slide'),
    'taxonomies' => array('category'),
    'rewrite' => array('slug' => 'slide'),
    'custom_fields' => true,
    'supports' => array('title', 'editor', 'author',
      'thumbnail', 'comments', 'custom_fields',)
  );
  register_post_type('slide', $args2);

  $args3 = array(
    'public' => true,
    'has_archive' => true,
    'labels' => array('name' => 'Ý kiến bệnh nhân'),
    'taxonomies' => array('category'),
    'rewrite' => array('slug' => 'ykienbenhnhan'),
    'custom_fields' => true,
    'supports' => array('title', 'editor', 'author',
      'thumbnail', 'comments', 'custom_fields')
  );

  register_post_type('ykienbenhnhan', $args3);


  $icon = array(
    'public' => true,
    'has_archive' => true,
    'labels' => array('name' => 'Biểu tượng bên dưới Hotline'),
    'taxonomies' => array(),
    'rewrite' => array('slug' => 'icon'),
    'custom_fields' => true,
    'supports' => array('title', 'editor',
      'thumbnail',),
  );
  register_post_type('icon', $icon);

  $dvkcb = array(
    'public' => true,
    'has_archive' => true,
    'labels' => array('name' => 'Dịch vụ khám chữa bệnh'),

    'rewrite' => array('slug' => 'dichvukhamchuabenh'),
    'custom_fields' => true,
    'supports' => array('title', 'editor',
      'thumbnail')
  );

  register_post_type('dichvukhamchuabenh', $dvkcb);
}


if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Tư vấn trực tuyến',
    'id' => 'tuvantructuyen',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}


if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Tư vấn khám chữa bệnh',
    'id' => 'tuvankhamchuabenh',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}


if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Bản đồ',
    'id' => 'bando',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}

if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Footer 1',
    'id' => 'footer1',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}


if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Footer 2',
    'id' => 'footer2',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}

if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Footer 3',
    'id' => 'footer3',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}

if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Footer 4',
    'id' => 'footer4',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}


if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Sidebar trang LIÊN HỆ',
    'id' => 'sidelienhe',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}


function editContent($url, $text = "Sửa")
{
  $url = get_bloginfo('url') . $url;
  $idUser = get_current_user_id();
  if ($idUser == 1) { ?>
    <div class="edit-content"
         style="position: absolute; top: 10px; right: 10px; padding: 3px 10px; background: #5bc0de; border: 1px solid #ddd">
      <a style="color: white; font-weight: 500; text-decoration: none"
         href="<?php echo $url; ?>"> <?php echo $text; ?> </a>
    </div>
    <?php
  }
}


function custom_excerpt_length($length)
{
  return 40;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);


function add_custom_sizes()
{
  add_image_size('slideduoimenu', 1366, 440, true);
  add_image_size('slidedoingubs', 1366, 300, true);
  add_image_size('gg', 400, 300, false);

  add_image_size('size400x300', 400, 300, true);
  add_image_size('size230x290', 230, 290, true);
  add_image_size('size140x140', 140, 140, true);
  add_image_size('size730x380', 730, 380, true);

}

add_action('after_setup_theme', 'add_custom_sizes');


if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Sidebar trang tin tuc',
    'id' => 'sidebartintuc',
    'description' => 'Lets put some widgets into that sidebar ',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
}

// phan trang

function custom_pagination($numpages = '', $pagerange = '', $paged = '')
{

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   *
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if (!$numpages) {
      $numpages = 1;
    }
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
    'base' => get_pagenum_link(1) . '%_%',
    'format' => 'page/%#%',
    'total' => $numpages,
    'current' => $paged,
    'show_all' => False,
    'end_size' => 1,
    'mid_size' => $pagerange,
    'prev_next' => True,
    'prev_text' => __('&laquo;'),
    'next_text' => __('&raquo;'),
    'type' => 'plain',
    'add_args' => false,
    'add_fragment' => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
    echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
    echo $paginate_links;
    echo "</nav>";
  }

}

// end phan trang


function suabai()
{
  $idUser = get_current_user_id();
  if ($idUser == 1) { ?>
    <div class="editpost">
      <?php edit_post_link("Sửa bài viết"); ?>
    </div>
    <?php
  }
}

