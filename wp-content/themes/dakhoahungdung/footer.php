<div class="footer-wrapper">
  <?php editContent("/wp-admin/widgets.php", "Sửa Footer"); ?>
  <div class="container">
    <div class="div-footer">
      <div class="row">

        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('')) : else : ?>
        <?php endif; ?>

        <div class="footet-col footer-col-1 col-md-4 col-sm-6">
          <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 1')) : else : ?>
          <?php endif; ?>
        </div>
        <div class="footet-col footer-col-2 col-md-2 col-sm-6">
          <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 2')) : else : ?>
          <?php endif; ?>


          <div class="noidungfooter">
            <ul>
              <?php
              $info = [
                'post_type' => 'dichvukhamchuabenh',
                'posts_per_page' => -1, // default -1 (all)
                'orderby' => 'date', // defaul date
                'order' => 'DESC', // default DESC
                'post_status' => array('publish')
              ];
              $dichvukhamchuabenh = new WP_Query($info);
              ?>

              <?php if ($dichvukhamchuabenh->have_posts()) :
                echo '';
                while ($dichvukhamchuabenh->have_posts()) : $dichvukhamchuabenh->the_post(); ?>

                  <li><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></li>

                <?php endwhile;
                wp_reset_postdata();
                echo '';
              else : ?>
                <?php _e('Sorry, no posts matched your criteria.'); ?>
              <?php endif; ?>

            </ul>
          </div>


        </div>
        <div class="footet-col footer-col-3 col-md-3 col-sm-6">
          <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 3')) : else : ?>
          <?php endif; ?>
        </div>
        <div class="footet-col footer-col-4 col-md-3 col-sm-6">
          <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 4')) : else : ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">

  <div class="div-copyright">
    Copyright &copy; 2017 Phòng khám Hưng Dũng. All Rights Reserved.
  </div>
</div>
</div> <!-- /container-fluid --?


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery1.12.4.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php bloginfo('template_url'); ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/slick/slick/slick.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>



