$(document).ready(function () {
    $('.slide-doingubacsi').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
    });

    $('.slide-duoi-menu').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
    });


    $('.slide-ykien-bn').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear', autoplay: true,
        autoplaySpeed: 2000,
    });


    $('[data-toggle="tooltip"]').tooltip();

})
