<?php
/**
 * The template for displaying page tintuc
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package DaKhoaHungDung
 */

get_header(); ?>

  <div class="row trangtintuc-wrapper">


    <div class="container">
      <div class="div-main col-md-8">
        <div class="tieude-tintuc">
          Tin tức
        </div>

        <?php
        if (function_exists('suabai')) {
          suabai();
        }
        ?>

        <?php
        while (have_posts()) : the_post(); ?>


          <div class="tieude-tintuc-chitiet">
            <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
          </div>

          
          <div class="anhtintic">

            <?php the_post_thumbnail('size730x380'); ?>
          </div>
          
          <div class="tintuc-meta">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <span class="span-ngay"> <?php the_time("d-m-Y"); ?> </span>
            <span> <?php the_time("g:i"); ?> </span>
          </div>

          <div class="noidung-tintucchitiet">
            <?php the_content(); ?>
          </div>


          <?php

//                    the_post_navigation();

          // If comments are open or we have at least one comment, load up the comment template.
//                    if (comments_open() || get_comments_number()) :
//                        comments_template();
//                    endif;

        endwhile; // End of the loop.
        ?>


        <div class="baivietlienquan-wrapper clearfix">
          <div class="tieude-bvlq">
            Bài viết liên quan
          </div>

          <div class="cacbaiviet">
            <?php
            $postid = get_the_ID();
            $info = [
              'post__not_in' => array($postid),
              'post_type' => 'tintuc',
              'post_status' => 'publish',
              'posts_per_page' => 4
            ];
            $tinlienquan = new WP_Query($info);
            ?>

            <?php if ($tinlienquan->have_posts()) :
              echo '';
              while ($tinlienquan->have_posts()) : $tinlienquan->the_post(); ?>

                <div class="col-sm-4 col-md-3 motbaiviet-lienquan">
                  <p><a href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('size140x140'); ?>
                    </a>
                  </p>

                  <div class="tieude">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                    </a>
                  </div>
                </div>

              <?php endwhile;
              wp_reset_postdata();
              echo '';
            else : ?>
              <?php _e('Sorry, no posts matched your criteria.'); ?>
            <?php endif; ?>

          </div>
        </div>

      </div>

      <div class="div-right col-md-4">

        <?php editContent("/wp-admin/widgets.php", "Sửa phần cột phải"); ?>
        <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar trang tin tuc')) : else : ?>
        <?php endif; ?>


      </div>
    </div>
  </div>


<?php
//get_sidebar();
get_footer();


