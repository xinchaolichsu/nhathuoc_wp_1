<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package DaKhoaHungDung
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main-style.css" type="text/css"
        media="screen"/>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/mediaquery.css" type="text/css"
        media="screen"/>

  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css"
        media="screen"/>

  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/font-awesome/css/font-awesome.min.css"
        type="text/css"
        media="screen"/>

  <link href="<?php bloginfo('template_url'); ?>/slick/slick/slick.css" rel="stylesheet">
  <link href="<?php bloginfo('template_url'); ?>/slick/slick/slick-theme.css" rel="stylesheet">

  <?php wp_head(); ?>


  <script>

  </script>

</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<div id="page" class="site">
  <a class="skip-link screen-reader-text"
     href="#content"><?php esc_html_e('Skip to content', 'dakhoahungdung'); ?></a>

  <div class="container-fluid">
    <div class="row">
      <div class="tren-menu">
        <div class="col-md-3 col-xs-12">
          <div class="div-logo">
            <a href="<?php bloginfo('url'); ?>">
              <img src="<?php bloginfo('template_url'); ?>/images/logo.PNG" alt="">
            </a>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="benphai-logo">
            <div class="bentren">
              Phòng khám đa khoa
            </div>
            <div class="benduoi">
              Hưng Dũng
            </div>
          </div>

        </div>

        <div class="col-md-6 col-xs-12">
          <div class="traosk-nhanniemtin">
            Trao sức khỏe, nhận niềm tin
          </div>
        </div>
      </div>
    </div>

    <div class="row ">
      <nav class="navbar navbar-default" role="navigation">


        <div class="container">
          <div class="searchform-wrapper">
            <?php editContent("/wp-admin/nav-menus.php", "Sửa Menu"); ?>
            <?php get_search_form(); ?>
          </div>
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php bloginfo('url'); ?>">Trang chủ</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <?php /* Primary navigation */
          wp_nav_menu(array(
            'theme_location' => 'mainmenu', //Menu location của bạn
            'depth' => 2, //Số cấp menu đa cấp
            'container' => 'div', //Thẻ bao quanh cặp thẻ ul
            'container_class' => 'collapse navbar-collapse navbar-ex1-collapse', //class của thẻ bao quanh cặp thẻ ul
            'menu_class' => 'nav navbar-nav', //class của thẻ ul
            'walker' => new wp_bootstrap_navwalker()) //Cái này để nguyên, không thay đổi
          );
          ?>
        </div>
      </nav>
    </div>


    <div class="row" style="position: relative">
      <?php editContent("/wp-admin/edit.php?category_name=slide_duoi_menu&post_type=slide", "Sửa slide"); ?>
      <div class="slide-duoi-menu" style="">

        <?php
        $info = [
          'post_type' => array(
            'slide',
          ),
          'category_name' => 'slide_duoi_menu',
        ];
        $slideDuoimenu = new WP_Query($info);
        ?>

        <?php if ($slideDuoimenu->have_posts()) :
          echo '';
          while ($slideDuoimenu->have_posts()) : $slideDuoimenu->the_post(); ?>

            <div>

              <?php the_post_thumbnail('slideduoimenu'); ?>

            </div>

          <?php endwhile;
          wp_reset_postdata();
          echo '';
        else : ?>
          <?php _e('Sorry, no posts matched your criteria.'); ?>
        <?php endif; ?>

      </div>
    </div>


    <div class="row">
      <div class="hotline">
        Hotline: 096.182.8228 / 091.726.8664
      </div>
    </div>

    <div class="row">
      <div class="duoi-hotline" style="position: relative">
        <?php editContent("/wp-admin/edit.php?post_type=icon", "Sửa Biểu tượng"); ?>

        <?php
        $info = [
          'post_type' => 'icon',
          'posts_per_page' => -1, // default -1 (all)
          'orderby' => 'date', // defaul date
          'order' => 'ASC', // default DESC
          'post_status' => array('publish')

        ];
        $icons = new WP_Query($info);
        ?>

        <?php if ($icons->have_posts()) :
          echo '';
          while ($icons->have_posts()) : $icons->the_post(); ?>


            <div class="div-icon">
              <a data-toggle="tooltip" data-placement="top" title="<?php the_field('tooltiptext', false, false); ?>"
                 href="<?php the_permalink(); ?>"> <?php the_post_thumbnail(); ?> </a>

              <div class="text-center text-uppercase duoiicon">
                <a href="<?php the_permalink(); ?>"> <?php the_field('tooltiptext', false, false); ?> </a>
              </div>
            </div>

          <?php endwhile;
          wp_reset_postdata();
          echo '';
        else : ?>
          <?php _e('Sorry, no posts matched your criteria.'); ?>
        <?php endif; ?>

      </div>
      <div class="duoi-hotline2">
        <!--                <img src="-->
        <?php //bloginfo('template_url'); ?><!--/images/duoihotline.PNG" alt="">-->


      </div>
    </div>

