<?php
get_header(); ?>

    <div class="row trangtintuc-wrapper">


        <div class="container">
            <div class="div-main col-md-8">

                <?php
                if (function_exists('suabai')) {
                    suabai();
                }
                ?>

                <?php
                while (have_posts()) : the_post(); ?>


                    <div class="tieude-tintuc-chitiet">
                        <?php the_title(); ?>
                    </div>


                    <div class="noidung-tintucchitiet">
                        <?php the_content(); ?>
                    </div>
                    <?php

                endwhile; // End of the loop.
                ?>

            </div>

            <div class="div-right col-md-4">

                <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar trang tin tuc')) : else : ?>
                <?php endif; ?>


            </div>
        </div>
    </div>


<?php
//get_sidebar();
get_footer();






